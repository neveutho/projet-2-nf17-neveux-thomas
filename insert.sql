insert into Site_géographique values ('Dongxing', 'Basic Industries');
insert into Site_géographique values ('Medveda', 'Finance');
insert into Site_géographique values ('Estela', 'n/a');
insert into Site_géographique values ('Ōdachō-ōda', 'Miscellaneous');

insert into Bâtiment  values ('SCS', 56.5, -25.7333842, -59.1078529, 'http://dummyimage.com/183x237.jpg/ff4444/ffffff', 5,'Dongxing');
insert into Bâtiment  values ('VIVO', 92.9, 33.786489, 133.7255189, 'http://dummyimage.com/249x218.png/cc0000/ffffff', 3,'Dongxing');
insert into Bâtiment  values ('LL', 2.4, 47.1910839, 18.4095578, 'http://dummyimage.com/163x118.bmp/dddddd/000000', 6,'Dongxing');
insert into Bâtiment  values ('JMT', 55.0, 26.444341, 115.699778, 'http://dummyimage.com/229x130.bmp/dddddd/000000', 6,'Estela');
insert into Bâtiment  values ('CYAD', 66.3, 47.0765556, 15.5109999, 'http://dummyimage.com/222x146.jpg/5fa2dd/ffffff', 3,'Estela');
insert into Bâtiment  values ('MYOK', 14.4, 27.3306951, 103.7137773, 'http://dummyimage.com/129x249.png/5fa2dd/ffffff', 3,'Dongxing');
insert into Bâtiment  values ('CYRXW', 42.0, 32.165, 108.8822222, 'http://dummyimage.com/201x155.png/ff4444/ffffff', 5,'Medveda');

insert into Salle values ('MNR^C', 23.5, 73, 'http://dummyimage.com/157x135.bmp/dddddd/000000', 2,'SCS');
insert into Salle values ('MTX', 23.1, 75, 'http://dummyimage.com/197x172.bmp/dddddd/000000', 4,'SCS');
insert into Salle values ('AGM^B', 1.0, 79, 'http://dummyimage.com/130x144.bmp/dddddd/000000', 5,'SCS');
insert into Salle values ('IRDMB', 6.5, 58, 'http://dummyimage.com/213x141.bmp/ff4444/ffffff', 2,'SCS');
insert into Salle values ('IFF', 16.2, 3, 'http://dummyimage.com/239x244.png/ff4444/ffffff', 2,'VIVO');
insert into Salle values ('QHC', 27.9, 51, 'http://dummyimage.com/130x223.jpg/ff4444/ffffff', 4,'VIVO');
insert into Salle values ('CVRR', 3.4, 50, 'http://dummyimage.com/131x220.bmp/dddddd/000000', 3,'SCS');
insert into Salle values ('AIC', 18.2, 38, 'http://dummyimage.com/192x185.png/dddddd/000000', 1,'JMT');
insert into Salle values ('AHPAU', 27.3, 2, 'http://dummyimage.com/238x166.bmp/5fa2dd/ffffff', 2,'VIVO');
insert into Salle values ('JJSF', 5.2, 1, 'http://dummyimage.com/214x216.png/5fa2dd/ffffff', 1,'JMT');

insert into Photo values ('http://dummyimage.com/148x169.png/dddddd/000000','IFF');
insert into Photo values ('http://dummyimage.com/243x155.png/cc0000/ffffff','IFF');
insert into Photo values ('http://dummyimage.com/106x184.bmp/cc0000/ffffff','IFF');
insert into Photo values ('http://dummyimage.com/225x178.bmp/dddddd/000000','JJSF');
insert into Photo values ('http://dummyimage.com/159x146.jpg/dddddd/000000','AHPAU');

insert into Caractéristique_technique (description, nb, salle) values ('prises électriques', 4, 'AHPAU');
insert into Caractéristique_technique (description, nb, salle) values ('prises réseaux', 3, 'AHPAU');
insert into Caractéristique_technique (description, nb, salle) values ('prises électriques', 2, 'AIC');
insert into Caractéristique_technique (description, nb, salle) values ('fenêtres', 3, 'AHPAU');
insert into Caractéristique_technique (description, nb, salle) values ('fenêtres', 4, 'IFF');

insert into  Employé (nom, prénom, num_badge, email, statut, bureau) values ('Giblett', 'Dorthy', 1, 'dgiblett0@rakuten.co.jp', 'Freelance', 'AIC');
insert into  Employé (nom, prénom, num_badge, email, statut, bureau) values ('Lissaman', 'Mil', 2, 'mlissaman1@t-online.de', 'CDD', 'IFF');
insert into  Employé (nom, prénom, num_badge, email, statut, bureau) values ('Loachhead', 'Casi', 3, 'cloachhead2@forbes.com', 'Freelance', 'JJSF');
insert into  Employé (nom, prénom, num_badge, email, statut, bureau) values ('Farleigh', 'Faulkner', 4, 'ffarleigh3@mit.edu', 'CDI', 'JJSF');
insert into  Employé (nom, prénom, num_badge, email, statut, bureau) values ('Gouthier', 'Caron', 5, 'cgouthier4@berkeley.edu', 'CDI', 'AHPAU');
insert into  Employé (nom, prénom, num_badge, email, statut, bureau) values ('Alejandre', 'Mildred', 6, 'malejandre5@chronoengine.com', 'CDI', 'JJSF');
insert into  Employé (nom, prénom, num_badge, email, statut, bureau) values ('Simunek', 'Latashia', 7, 'lsimunek6@yahoo.co.jp', 'CDI', 'AIC');
insert into  Employé (nom, prénom, num_badge, email, statut, bureau) values ('Stoffersen', 'Nell', 8, 'nstoffersen7@typepad.com', 'CDD', 'IFF');
insert into  Employé (nom, prénom, num_badge, email, statut, bureau) values ('Bloss', 'Joyce', 9, 'jbloss8@yellowpages.com', 'Stagiaire', 'AHPAU');
insert into  Employé (nom, prénom, num_badge, email, statut, bureau) values ('Batts', 'Hallie', 10, 'hbatts9@kickstarter.com', 'CDI', 'AHPAU');

insert into Machine (type, id, modèle, description, puissance_élec, triphasé, réseau, num_maintenance, entreprise_maintenance, gaz_requis, largeur, longueur, hauteur, salle) values ('laboratoire', 1, 'luid78', 'Open-architected upward-trending benchmark', 2.3, true, false, 131752, 'Blackrock MuniHoldings', 'O2', 12.6, 24.8, 18.1, 'IFF');
insert into Machine (type, id, modèle, description, puissance_élec, triphasé, réseau, num_maintenance, entreprise_maintenance, gaz_requis, largeur, longueur, hauteur, salle) values ('fabrication', 2, 'coco77', 'Multi-tiered intermediate pricing structure', 16.4, false, true, 877626, 'Eagle Company Inc.', 'Méthane', 9.8, 23.3, 29.1, 'IFF');
insert into Machine (type, id, modèle, description, puissance_élec, triphasé, réseau, num_maintenance, entreprise_maintenance, gaz_requis, largeur, longueur, hauteur, salle) values ('fabrication', 3, 'luid78', 'Front-line leading edge middleware', 17.0, true, false, 921586, 'Capital City Bank Group', 'Méthane', 1.0, 11.1, 27.1, 'AHPAU');
insert into Machine (type, id, modèle, description, puissance_élec, triphasé, réseau, num_maintenance, entreprise_maintenance, gaz_requis, largeur, longueur, hauteur, salle) values ('fabrication', 4, 'bipbop15', 'Digitized optimal standardization', 28.6, false, false, 568892, 'PowerShares International', 'O2', 17.7, 13.9, 14.2, 'IFF');
insert into Machine (type, id, modèle, description, puissance_élec, triphasé, réseau, num_maintenance, entreprise_maintenance, gaz_requis, largeur, longueur, hauteur, salle) values ('fabrication', 5, 'bipbop15', 'Down-sized empowering customer loyalty', 21.3, true, true, 473908, 'Group 1 Automotive, Inc.', 'O2', 16.7, 11.0, 5.4, 'AIC');
insert into Machine (type, id, modèle, description, puissance_élec, triphasé, réseau, num_maintenance, entreprise_maintenance, gaz_requis, largeur, longueur, hauteur, salle) values ('laboratoire', 6, 'coco77', 'Focused encompassing workforce', 25.2, true, false, 484211, 'Condor Hospitality Trust, Inc.', 'Azote', 1.3, 20.2, 23.0, 'AHPAU');
insert into Machine (type, id, modèle, description, puissance_élec, triphasé, réseau, num_maintenance, entreprise_maintenance, gaz_requis, largeur, longueur, hauteur, salle) values ('laboratoire', 7, 'bipbop15', 'Implemented user-facing instruction set', 49.8, true, true, 796634, 'JMP Group LLC', 'O2', 17.2, 14.5, 24.7, 'AHPAU');
insert into Machine (type, id, modèle, description, puissance_élec, triphasé, réseau, num_maintenance, entreprise_maintenance, gaz_requis, largeur, longueur, hauteur, salle) values ('fabrication', 8, 'coco77', 'Intuitive foreground flexibility', 30.9, true, true, 967937, 'Merrill Lynch Depositor, Inc.', 'Méthane', 7.8, 20.0, 0.1, 'AHPAU');
insert into Machine (type, id, modèle, description, puissance_élec, triphasé, réseau, num_maintenance, entreprise_maintenance, gaz_requis, largeur, longueur, hauteur, salle) values ('fabrication', 9, 'luid78', 'Self-enabling user-facing customer loyalty', 12.2, false, true, 430187, 'Golar LNG Partners LP', 'Méthane', 2.9, 13.1, 6.0, 'IFF');
insert into Machine (type, id, modèle, description, puissance_élec, triphasé, réseau, num_maintenance, entreprise_maintenance, gaz_requis, largeur, longueur, hauteur, salle) values ('fabrication', 10, 'coco77', 'Function-based exuding standardization', 44.5, true, false, 699915, 'PPlus Trust', 'Méthane', 17.1, 5.3, 8.9, 'IFF');

insert into Modèle_téléphone (nom, technologie, marque) values ('JEDJ', 'VOIP', 'motorola');
insert into Modèle_téléphone (nom, technologie, marque) values ('AHGAG', 'TOIP', 'huawei');
insert into Modèle_téléphone (nom, technologie, marque) values ('HZDVZ', 'Landline', 'nokia');

insert into Poste_téléphonique (num_interne, modèle, salle, possesseur) values (1305235, 'JEDJ', 'AHPAU', 7);
insert into Poste_téléphonique (num_interne, modèle, salle, possesseur) values (9334086, 'HZDVZ', 'AIC', 3);
insert into Poste_téléphonique (num_interne, modèle, salle, possesseur) values (4009803, 'AHGAG', 'IFF', 1);
insert into Poste_téléphonique (num_interne, modèle, salle, possesseur) values (3066474, 'JEDJ', 'JJSF', 3);
insert into Poste_téléphonique (num_interne, modèle, salle, possesseur) values (2759857, 'AHGAG', 'AHPAU', 9);
insert into Poste_téléphonique (num_interne, modèle, salle, possesseur) values (4219297, 'HZDVZ', 'AIC', 9);
insert into Poste_téléphonique (num_interne, modèle, salle, possesseur) values (9713492, 'JEDJ', 'IFF', 10);

insert into Moyen_informatique (nom, OS, type, pc_de_fonction, responsable, machine_liée, salle) values ('AGM^A', 'Ied admiral', 'PC_fixe', 4, 4, 5, 'JJSF');
insert into Moyen_informatique (nom, OS, type, pc_de_fonction, responsable, machine_liée, salle) values ('ENS', 'Foxred', 'PC_portable', 10, 1, 2, 'AHPAU');
insert into Moyen_informatique (nom, OS, type, pc_de_fonction, responsable, machine_liée, salle) values ('JONE', 'Agama lizard', 'serveur', 5, 4, 3, 'AIC');
insert into Moyen_informatique (nom, OS, type, pc_de_fonction, responsable, machine_liée, salle) values ('CLX', 'African polecat', 'serveur', 5, 10, 2, 'JJSF');
insert into Moyen_informatique (nom, OS, type, pc_de_fonction, responsable, machine_liée, salle) values ('IMMY', 'Dragon, komodo', 'PC_portable', 2, 7, 1, 'JJSF');
insert into Moyen_informatique (nom, OS, type, pc_de_fonction, responsable, machine_liée, salle) values ('MAA^I', 'Skink, african', 'PC_fixe', 2, 1, 5, 'AIC');
insert into Moyen_informatique (nom, OS, type, pc_de_fonction, responsable, machine_liée, salle) values ('FCF', 'American sheep', 'PC_fixe', 9, 7, 5, 'JJSF');
insert into Moyen_informatique (nom, OS, type, pc_de_fonction, responsable, machine_liée, salle) values ('BPFHP', 'Blue shark', 'PC_fixe', 3, 3, 4, 'AIC');
insert into Moyen_informatique (nom, OS, type, pc_de_fonction, responsable, machine_liée, salle) values ('RSO^C', 'Mongoose', 'PC_portable', 4, 2, 5, 'IFF');
insert into Moyen_informatique (nom, OS, type, pc_de_fonction, responsable, machine_liée, salle) values ('LNC.WS', 'Yellow sungazer', 'PC_fixe', 8, 1, 6, 'AIC');

insert into Adresse_élec (adresse, type, moyen_informatique) values ('234.89.176.152', 'Ethernet', 'AGM^A');
insert into Adresse_élec (adresse, type, moyen_informatique) values ('116.35.231.69', 'Ethernet', 'JONE');
insert into Adresse_élec (adresse, type, moyen_informatique) values ('66.113.91.29', 'Ethernet', 'FCF');
insert into Adresse_élec (adresse, type, moyen_informatique) values ('214.234.163.1', 'MAC', 'LNC.WS');
insert into Adresse_élec (adresse, type, moyen_informatique) values ('107.132.7.101', 'MAC', 'AGM^A');
insert into Adresse_élec (adresse, type, moyen_informatique) values ('106.20.23.45', 'IP', 'JONE');
insert into Adresse_élec (adresse, type, moyen_informatique) values ('226.59.106.206', 'IP', 'FCF');
insert into Adresse_élec (adresse, type, moyen_informatique) values ('157.43.20.239', 'IP', 'LNC.WS');
insert into Adresse_élec (adresse, type, moyen_informatique) values ('233.123.218.37', 'MAC', 'JONE');
insert into Adresse_élec (adresse, type, moyen_informatique) values ('28.200.80.142', 'MAC', 'FCF');

insert into Laboratoire (sigle, nom, logo, directeur,Thématique_étude) values ('AMAG', 'Hog''s-fennel', 'https://robohash.org/sitida.png?size=50x50&set=set1', 7,'["Professor","Raclette","Materiaux"]');
insert into Laboratoire (sigle, nom, logo, directeur,Thématique_étude) values ('HDB', 'Cade Juniper', 'https://robohash.org/consequaturdoloremquepraesentium.bmp?size=50x50&set=set1', 2,'["Poker","Chaussures"]');
insert into Laboratoire (sigle, nom, logo, directeur) values ('RE', 'Perezia', 'https://robohash.org/sapienteerrorvoluptas.bmp?size=50x50&set=set1', 2);
insert into Laboratoire (sigle, nom, logo, directeur) values ('LIVN', 'Spike-primrose', 'https://robohash.org/eumevenietoccaecati.bmp?size=50x50&set=set1', 2);
insert into Laboratoire (sigle, nom, logo, directeur,Thématique_étude) values ('STLRU', 'Tansyaster', 'https://robohash.org/laudantiumcupiditatevoluptas.bmp?size=50x50&set=set1', 4,'["Hotels","Raclette","Environmental"]');
insert into Laboratoire (sigle, nom, logo, directeur,Thématique_étude) values ('DPS', 'Ellipticleaf', 'https://robohash.org/etnonvoluptas.jpg?size=50x50&set=set1', 1,'["Data","Plastique","Materiaux"]');
insert into Laboratoire (sigle, nom, logo, directeur) values ('ANGO', 'Snakeweed', 'https://robohash.org/utquiut.jpg?size=50x50&set=set1', 1);
insert into Laboratoire (sigle, nom, logo, directeur) values ('PVTB', 'Lilythorn', 'https://robohash.org/quosuntdistinctio.png?size=50x50&set=set1', 10);
insert into Laboratoire (sigle, nom, logo, directeur) values ('GNMA', 'Woollyheads', 'https://robohash.org/beataeetdeleniti.bmp?size=50x50&set=set1', 6);
insert into Laboratoire (sigle, nom, logo, directeur) values ('AP', 'Cinquefoil', 'https://robohash.org/ipsamdignissimosblanditiis.png?size=50x50&set=set1', 2);


insert into Département (sigle, nom, directeur,Domaine_expertise) values ('CUNB', 'Legal', 3,'["Clothing","Beauty"]');
insert into Département (sigle, nom, directeur) values ('AFGE', 'Sales', 4);
insert into Département (sigle, nom, directeur,Domaine_expertise) values ('CRL', 'Marketing', 1,'["Industrial"]');
insert into Département (sigle, nom, directeur,Domaine_expertise) values ('SGBX', 'Business Development', 7,'["Outdoors","VTT"]');
insert into Département (sigle, nom, directeur,Domaine_expertise) values ('FIV', 'Training', 5,'["Industrial","Sports","Automotive"]');
insert into Département (sigle, nom, directeur) values ('HLF', 'Human Resources', 8);
insert into Département (sigle, nom, directeur,Domaine_expertise) values ('NL', 'Product Management', 1,'["Outdoors"]');
/*
insert into  Thématique_étude (nom, labo) values ( 'Professor', 'AMAG');
insert into  Thématique_étude (nom, labo) values ( 'Poker', 'RE');
insert into  Thématique_étude (nom, labo) values ( 'Electric Utilities', 'AP');
insert into  Thématique_étude (nom, labo) values ( 'Hotels/Resorts', 'GNMA');
insert into  Thématique_étude (nom, labo) values ( 'Environmental', 'ANGO');
insert into  Thématique_étude (nom, labo) values ( 'Data', 'DPS');
insert into  Thématique_étude (nom, labo) values ( 'Raclette', 'AMAG');

insert into Domaine_expertise (nom, département) values ('Clothing', 'CUNB');
insert into Domaine_expertise (nom, département) values ('Industrial', 'CRL');
insert into Domaine_expertise (nom, département) values ('Books', 'HLF');
insert into Domaine_expertise (nom, département) values ('Outdoors', 'NL');
insert into Domaine_expertise (nom, département) values ('Sports', 'FIV');
insert into Domaine_expertise (nom, département) values ('Automotive', 'SGBX');
insert into Domaine_expertise (nom, département) values ('Beauty', 'CUNB');
*/
insert into Projet (sigle, nom, start_date, end_date, chef_projet, département, labo) values ('GABC', '"Zaam-Dox"', '7-4-2019', '11-6-2019', 2, 'CUNB', 'AMAG');
insert into Projet (sigle, nom, start_date, end_date, chef_projet, département, labo) values ('CRL', '"Flowdesk"', '10-2-2019', '7-9-2021', 4, 'CRL', 'RE');
insert into Projet (sigle, nom, start_date, end_date, chef_projet, département, labo) values ('ACHC', '"Greenlam"', '3-7-2020', '9-2-2021', 2, 'HLF', 'AP');
insert into Projet (sigle, nom, start_date, end_date, chef_projet, département, labo) values ('OACQ', '"Pannier"', '11-2-2019', '5-4-2020', 6, 'NL', 'GNMA');
insert into Projet (sigle, nom, start_date, end_date, chef_projet, département, labo) values ('OCLR', '"Duobam"', '4-9-2020', '4-3-2020', 6, 'FIV', 'ANGO');
insert into Projet (sigle, nom, start_date, end_date, chef_projet, département, labo) values ('EMCG', '"Transcof"', '3-2-2020', '12-6-2019', 10, 'SGBX', 'DPS');
insert into Projet (sigle, nom, start_date, end_date, chef_projet, département, labo) values ('DFS^B', '"Ventosanzap"', '4-4-2020', '5-5-2022', 1, 'CUNB', 'AMAG');

insert into  Acteur (acteur, rôle, projet) values (7, '"Therapist"', 'GABC');
insert into  Acteur (acteur, rôle, projet) values (8, '"Software Consultant"', 'CRL');
insert into  Acteur (acteur, rôle, projet) values (4, '"Community Outreach"', 'ACHC');
insert into  Acteur (acteur, rôle, projet) values (6, '"Nurse Practicioner"', 'OCLR');
insert into  Acteur (acteur, rôle, projet) values (2, '"Quality Engineer"', 'GABC');
insert into  Acteur (acteur, rôle, projet) values (9, '"Technical Writer"', 'CRL');
insert into  Acteur (acteur, rôle, projet) values (9, '"Systems Manager"', 'ACHC');
