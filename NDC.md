# Note de Clarification

Version 1
Le 26/05/2020

## Contexte 

Le projet “Racoon Smart City” est la conception d'un système de gestion du patrimoine humain, immobilier et industriel  pour une entreprise un secteur médical et cosmétique.

## Acteurs

- Maîtrise d’ouvrage: l'entreprise « Umbrella Corporation »

- Maîtrise d’oeuvre:  Thomas Neveux

## Objet du projet

Permettre l'entreprise la gestion de la totalité de son patrimoine humain, immobilier et industriel pour pouvoir connaître l'état de l'entreprise en temps réel

## Livrables attendus

- La Note de clarification
- Le MCD
- Le MLD
- Les fichiers de code du projet en SQL

## Objets
- Site géographique
- Bâtiment
- Salle
- Employé
- Machine
- Poste téléphonique
- Moyen informatique
- Laboratoire
- Département
- Projet
- Relation Hiérarchique


## Propriétés 

### Site_géographique :
    nom {key} (généralement celui de son département géographique ou de la ville dans laquelle il est situé)
    fonction (siège social, production, recherche...).

### Bâtiment :
    nom (unique)
    superficie
    latitude
    longitude
    plan
    nombre d'étages
    appartient à un site géographique.

### Salle :
    nom (unique)
    superficie
    capacité humaine maximale
    photos (il peut y avoir plusieurs photos pour une salle,une table à part sera donc nécessaire)
    plan de salle
    caractéristiques techniques (étant nombreuses et diverses, elle seront dans une table à part)
    appartient à un bâtiment
    étage du bâtiment

### Employé :
    nom
    prénom
    numéro de badge (unique)
    e-mail
    statut employé (CDI, CDD, Stagiaire, etc.)
    emplacement du bureau (salle)
    peut être membre d’un département, ainsi que 
    peut être membre d‘un laboratoire (un employé ne peut appartenir qu’à un département/laboratoire, on peut donc placer ces attributs dans la table employé et ne pas créer de tables pour les relations département-employé et laboratoire-employé).

### Machine :
    type (fabrication ou laboratoire)
    identifiant unique
    modèle, une description
    puissance électrique
    besoin ou non du triphasé
    besoin ou non de réseau
    numéro contrat maintenance
    entreprise de maintenance
    besoin ou non d'un gaz spécifique
    dimensions (largeur, longueur, hauteur) en mètres dans le cas des grosses machines (des dimensions null signifient que la machine est “petite”) 
    est dans une salle

### Poste téléphonique :
    numéro interne (unique)
    technologie (VOIP, TOIP ,Landline, etc...)
    modèle de téléphone
    marque
    potentiellement un possesseur (null sinon). 
    est dans une salle

### Moyen informatique :
    nom (unique)
    type (PC fixe, portable ou serveur)
    OS
    potentiellement un responsable
    peut être lié à un projet
    peut être lié à une machine
    peut être l’ordinateur de fonction d’un employé
    est dans une salle
    peut posséder aucune ou plusieurs adresses électroniques qui seront dans une table à part (une relation moyen informatique-adresse électronique a une adresse unique, un type (MAC, IP, Wifi/ Ethernet...) et une machine associée)

### Laboratoire :
    nom
    signe (unique)
    logo
    directeur
    peut posséder aucune ou plusieurs  thématiques d'étude qui seront dans une table à part (une relation  laboratoire-thématique d'étude a un identifiant unique, un nom de thématique d’étude et un laboratoire associé)

### Département :
    nom
    sigle (unique)
    directeur
    peut posséder aucun ou plusieurs domaines d'expertise qui seront dans une table à part (une relation  département-domaine d’expertise a un identifiant unique, un nom de domaine d’expertise et un département associé). 

### Projet :
    nom
    sigle (unique)
    chef de projet
    start_date
    end_date
    peut être lié à un département
    peut être lié à un laboratoire
    acteurs, qui seront dans une table à part (une relation  projet-acteur a un identifiant unique, un sigle de projet, un employé et un rôle).
    

## Contraintes
Une relation hiérarchique implique 2 employés qui doivent être différents.

Un employé ne peut appartenir qu’à un département et à un laboratoire au maximum.

Les laboratoires peuvent avoir aucune ou plusieurs thématiques d’étude.

Les départements peuvent avoir aucun ou plusieurs domaines d'expertise.

Les moyens informatiques peuvent avoir aucune ou plusieurs adresses électroniques.

L’étage d’une salle doit être inférieur ou égal au nombre d’étages du bâtiment dans lequel elle se situe.


## Utilisateur
On suppose que l’utilisateur est un membre du siège social qui désire avoir un point de vue global sur l’entreprise, 
ou un membre des ressources humaines qui désire des informations sur les employés et leurs fonctions dans l’entreprise. 
L’utilisateur a donc le droit de consulter et d’ajouter des données (droits administrateur)


## Vues
- Vue de tous les employés sans projet.

- Classement décroissant des employés par nombre de moyens informatiques dont ils sont responsable.

- Classement décroissant des employés par nombre de poste de direction / responsable.

- Vue des salles en sur-effectif.

- Vue sur tout ce que contient une salle (matériel comme employés).

- Vue sur tous les projets en cours.




