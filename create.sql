DROP TABLE IF EXISTS Site_géographique CASCADE;
DROP TABLE IF EXISTS Bâtiment CASCADE;
DROP TABLE IF EXISTS Salle CASCADE;
DROP TABLE IF EXISTS Photo CASCADE;
DROP TABLE IF EXISTS Caractéristique_technique CASCADE;
DROP TABLE IF EXISTS Employé CASCADE;
DROP TABLE IF EXISTS Machine CASCADE;
DROP TABLE IF EXISTS Poste_téléphonique CASCADE;
DROP TABLE IF EXISTS Modèle_téléphone CASCADE;
DROP TABLE IF EXISTS Moyen_informatique CASCADE;
DROP TABLE IF EXISTS Adresse_élec CASCADE;
DROP TABLE IF EXISTS Laboratoire CASCADE;
DROP TABLE IF EXISTS Thématique_étude CASCADE;
DROP TABLE IF EXISTS Département CASCADE;
DROP TABLE IF EXISTS Domaine_expertise CASCADE;
DROP TABLE IF EXISTS Projet CASCADE;
DROP TABLE IF EXISTS Acteur CASCADE;


CREATE TABLE Site_géographique (
nom varchar(30) PRIMARY KEY,
fonction varchar(30) NOT NULL
);

CREATE TABLE Bâtiment (
nom varchar(20) PRIMARY KEY,
superficie float NOT NULL,
latitude float NOT NULL,
longitude float NOT NULL,
plan varchar(100),
nb_étages int NOT NULL,
site_géo varchar(30) NOT NULL,
FOREIGN KEY (site_géo) REFERENCES Site_géographique(nom)
);

CREATE TABLE Salle (
nom varchar(20) PRIMARY KEY,
superficie float NOT NULL,
capacité_humaine_max int NOT NULL,
plan varchar(100),
étage int NOT NULL,
bâtiment varchar(20) NOT NULL,
FOREIGN KEY (bâtiment) REFERENCES Bâtiment(nom)
);

CREATE TABLE Photo (
url varchar(100) PRIMARY KEY,
salle varchar(20) NOT NULL,
FOREIGN KEY (salle) REFERENCES Salle(nom)
);

CREATE TABLE Caractéristique_technique(
id serial PRIMARY KEY,
description varchar(50) NOT NULL,
nb int,
salle varchar(20) NOT NULL,
FOREIGN KEY (salle) REFERENCES Salle(nom)
);

CREATE TABLE Employé(
nom varchar(20) NOT NULL, 
prénom varchar(20) NOT NULL,
num_badge int PRIMARY KEY, 
email  varchar(50) NOT NULL, 
statut varchar(20) NOT NULL, 
labo varchar(20),
département varchar(20),
bureau varchar(20),
FOREIGN KEY (bureau) REFERENCES Salle(nom)
);

CREATE TABLE Machine (
type text NOT NULL CHECK ( type IN ('fabrication', 'laboratoire')), 
id serial PRIMARY KEY,
modèle varchar(20) NOT NULL, 
description varchar(100), 
puissance_élec float NOT NULL,
triphasé bool NOT NULL, 
réseau bool NOT NULL, 
num_maintenance int NOT NULL, 
entreprise_maintenance varchar(30) NOT NULL, 
gaz_requis varchar(30), 
largeur float, 
longueur float, 
hauteur float, 
CONSTRAINT dim_null
CHECK ((largeur IS NULL AND longueur IS NULL AND hauteur IS NULL) OR (largeur IS NOT NULL AND longueur IS NOT NULL AND hauteur IS NOT NULL)) ,
salle varchar(20) NOT NULL,
FOREIGN KEY (salle) REFERENCES Salle(nom)
);

CREATE TABLE Modèle_téléphone (
nom varchar(20) PRIMARY KEY, 
technologie varchar(20) NOT NULL, 
marque varchar(20) NOT NULL
);

CREATE TABLE Poste_téléphonique (
num_interne int PRIMARY KEY, 
modèle varchar(20) NOT NULL,
FOREIGN KEY (modèle) REFERENCES Modèle_téléphone(nom),
salle varchar(20) NOT NULL,
FOREIGN KEY (salle) REFERENCES Salle(nom),
possesseur int NOT NULL,
FOREIGN KEY(possesseur) REFERENCES Employé(num_badge)
);

CREATE TABLE Moyen_informatique (
nom varchar(20) PRIMARY KEY, 
type text NOT NULL CHECK ( type IN ('PC_fixe', 'PC_portable', 'serveur')), 
OS varchar(20), 
pc_de_fonction int,
FOREIGN KEY(pc_de_fonction) REFERENCES Employé(num_badge),
responsable int NOT NULL,
FOREIGN KEY(responsable) REFERENCES Employé(num_badge),
projet varchar(20),
machine_liée int,
FOREIGN KEY (machine_liée) REFERENCES Machine(id),
salle varchar(20) NOT NULL,
FOREIGN KEY (salle) REFERENCES Salle(nom)
);

CREATE TABLE Adresse_élec (
adresse varchar(100) UNIQUE, 
type varchar(20) NOT NULL, 
moyen_informatique varchar(20) NOT NULL,
FOREIGN KEY(moyen_informatique) REFERENCES Moyen_informatique(nom),
PRIMARY KEY(type,moyen_informatique)
);

CREATE TABLE Laboratoire (
sigle varchar(20) PRIMARY KEY, 
nom varchar(20) NOT NULL, 
logo varchar(100), 
directeur int NOT NULL,
FOREIGN KEY(directeur) REFERENCES Employé(num_badge),
Thématique_étude JSON
);

/*CREATE TABLE Thématique_étude (
nom varchar(20),
labo varchar(20),
FOREIGN KEY(labo) REFERENCES Laboratoire(sigle),
PRIMARY KEY (nom,labo)
);*/

CREATE TABLE Département (
sigle varchar(20) PRIMARY KEY, 
nom varchar(20) NOT NULL, 
directeur int NOT NULL,
FOREIGN KEY(directeur) REFERENCES Employé(num_badge),
Domaine_expertise JSON
);

/*CREATE TABLE Domaine_expertise (
nom varchar(20),
département varchar(20),
FOREIGN KEY(département) REFERENCES Département(sigle),
PRIMARY KEY (nom,département)
);*/

CREATE TABLE Projet (
sigle varchar(20) PRIMARY KEY, 
nom JSON NOT NULL, 
start_date Date NOT NULL, 
end_date Date NOT NULL, 
chef_projet int NOT NULL,
FOREIGN KEY(chef_projet) REFERENCES Employé(num_badge),
département varchar(20) ,
FOREIGN KEY(département) REFERENCES Département(sigle),
labo varchar(20),
FOREIGN KEY(labo) REFERENCES Laboratoire(sigle),
CHECK (département IS NOT NULL OR labo IS NOT NULL)
);

CREATE TABLE Acteur (
acteur int,
FOREIGN KEY(acteur) REFERENCES Employé(num_badge),
rôle JSON NOT NULL, 
projet varchar(20),
FOREIGN KEY(projet) REFERENCES Projet(sigle),
PRIMARY KEY (acteur,projet)
);

ALTER TABLE Employé
ADD FOREIGN KEY(labo) REFERENCES Laboratoire(sigle);

ALTER TABLE Employé
ADD FOREIGN KEY (département) REFERENCES Département(sigle);

ALTER TABLE Moyen_informatique
ADD FOREIGN KEY(projet) REFERENCES Projet(sigle);

