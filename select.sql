/*vue donnant tous les directeurs de laboratoire*/
DROP VIEW IF EXISTS  directeur_labo CASCADE;
CREATE VIEW directeur_labo AS
SELECT Employé.num_badge, Employé.nom, Employé.prénom, Laboratoire.nom as lab
FROM Employé,Laboratoire
WHERE Employé.num_badge = Laboratoire.directeur;

/*vue donnant tous les directeurs de département*/
DROP VIEW IF EXISTS  directeur_départ CASCADE;
CREATE VIEW directeur_départ AS
SELECT Employé.num_badge, Employé.nom, Employé.prénom, Département.nom as départ
FROM Employé, Département
WHERE Employé.num_badge = Département.directeur;

/*vue donnant tous les chefs de projet*/
DROP VIEW IF EXISTS  chef_projet CASCADE;
CREATE VIEW  chef_projet AS
SELECT Employé.num_badge, Employé.nom, Employé.prénom,  Projet.nom as projet
FROM Employé,Projet
WHERE Employé.num_badge = Projet.chef_projet;

/*vue donnant le classement décroissant des employés par nombre de poste de direction */
DROP VIEW IF EXISTS  nb_poste_direction CASCADE;
CREATE VIEW  nb_poste_direction AS
SELECT Employé.num_badge, Employé.nom, Employé.prénom, COALESCE(l.tot1,0)+COALESCE(d.tot2,0)+COALESCE(p.tot3,0) as nb_postes
FROM Employé 
LEFT JOIN(
SELECT COUNT(*) as tot1, Laboratoire.directeur
FROM Laboratoire
GROUP BY Laboratoire.directeur
) as l ON Employé.num_badge = l.directeur
LEFT JOIN(
SELECT COUNT(*) as tot2, Département.directeur
FROM Département
GROUP BY Département.directeur
) as d ON Employé.num_badge = d.directeur
LEFT JOIN(
SELECT COUNT(*) as tot3, Projet.chef_projet
FROM Projet
GROUP BY Projet.chef_projet
) as p ON Employé.num_badge = p.chef_projet
ORDER BY nb_postes DESC;


/*vue donnant tous les employés sans projet*/
DROP VIEW IF EXISTS  employé_sans_projet CASCADE;
CREATE VIEW employé_sans_projet AS
SELECT Employé.num_badge, Employé.nom, Employé.prénom
FROM Employé
EXCEPT
SELECT Employé.num_badge, Employé.nom, Employé.prénom 
FROM Employé,Acteur
WHERE Employé.num_badge = Acteur.acteur;

/*vue donnant le classement décroissant des employés par nombre de moyens informatiques dont ils sont responsable*/
DROP VIEW IF EXISTS  nb_resp_informatique CASCADE;
CREATE VIEW nb_resp_informatique AS
SELECT Employé.num_badge, Employé.nom, Employé.prénom, COUNT(*) as nb_moyen_info
FROM Employé, Moyen_informatique
WHERE Employé.num_badge = Moyen_informatique.responsable
GROUP BY Employé.num_badge
ORDER BY nb_moyen_info DESC;

/*Vue des salles en sureffectif*/
DROP VIEW IF EXISTS  salles_en_sureffectif CASCADE;
CREATE VIEW salles_en_sureffectif AS
SELECT Salle.nom, Salle.capacité_humaine_max, e2.nb_employé
FROM Salle JOIN(
SELECT COUNT(*) as nb_employé, Employé.bureau
FROM Employé
GROUP BY Employé.bureau
) as e2 ON e2.bureau=Salle.nom
WHERE Salle.capacité_humaine_max < nb_employé;

/*vue donnant tous les projets en cours (ici à la date '1-1-2020')*/
DROP VIEW IF EXISTS  projets_en_cours CASCADE;
CREATE VIEW projets_en_cours AS
SELECT *
FROM Projet
WHERE current_date < Projet.end_date;

/*vue donnant toutes les thématiques d'étude des laboratoires (exemple d'utilisation de JSON)*/
DROP VIEW IF EXISTS  thématiques_labo CASCADE;
CREATE VIEW thématiques_labo AS
SELECT sigle as sigle_labo, nom as nom_labo, a.* 
FROM Laboratoire,JSON_ARRAY_ELEMENTS(Laboratoire.thématique_étude) a ;

/*vue donnant toutes les domaine d’expertise des département (exemple d'utilisation de JSON)*/
DROP VIEW IF EXISTS domaine_expertise CASCADE;
CREATE VIEW domaine_expertise AS
SELECT sigle as sigle_labo, nom as nom_labo, a.* 
FROM Département,JSON_ARRAY_ELEMENTS(Département.domaine_expertise) a;


/*vue pour voir la contrainte Projection(Bâtiment,site_géo)= Projection(Site_géographique,nom). La vue est vide si la contrainte est respectée*/
DROP VIEW IF EXISTS contrainte_bat_site_géo CASCADE;
CREATE VIEW contrainte_bat_site_géo AS
SELECT site_géo 
FROM Bâtiment
EXCEPT
SELECT nom
From Site_géographique;


/*vue pour voir la contrainte Projection(Salle,bâtiment)= Projection(Bâtiment,nom). La vue est vide si la contrainte est respectée*/
DROP VIEW IF EXISTS contrainte_salle_bat CASCADE;
CREATE VIEW contrainte_salle_bat AS
SELECT bâtiment 
FROM Salle
EXCEPT
SELECT nom
From Bâtiment;

